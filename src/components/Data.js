import { Link } from "react-router-dom";
export function Data(){
	return(
		<>
			<div className="content py-5 mt-2 mb-5">
            <div className="container px-sm-2">
               {/*Old data*/}
               <div className="row">
                  <div className="col-md-6 m-auto text-center">
                      <img src="/assets/image/old-data.png" className="img-forget" alt="" />
                        <Link to="/" className="card p-3 shadow border-0 w-100 mb-3">
                           <div className="forget_password d-flex justify-content-between">
                              <h4 className="mb-0">Sep 2023</h4>
                              <img src="/assets/image/arrow-right.svg" style={{width:'30px'}} alt="" />
                           </div>
                        </Link>
                        <Link to="/" className="card p-3 shadow border-0 w-100 mb-3">
                           <div className="forget_password d-flex justify-content-between">
                              <h4 className="mb-0">May 2023</h4>
                              <img src="/assets/image/arrow-right.svg" style={{width:'30px'}} alt="" />
                           </div>
                        </Link>
                        <Link to="/" className="card p-3 shadow border-0 w-100 mb-3">
                           <div className="forget_password d-flex justify-content-between">
                              <h4 className="mb-0">Feb 2023</h4>
                              <img src="/assets/image/arrow-right.svg" style={{width:'30px'}} alt="" />
                           </div>
                        </Link>
                        <Link to="/" className="card p-3 shadow border-0 w-100 mb-3">
                           <div className="forget_password d-flex justify-content-between">
                              <h4 className="mb-0">Oct 2022</h4>
                              <img src="/assets/image/arrow-right.svg" style={{width:'30px'}} alt="" />
                           </div>
                        </Link>
                        <Link to="/" className="card p-3 shadow border-0 w-100 mb-3">
                           <div className="forget_password d-flex justify-content-between">
                              <h4 className="mb-0">May 2022</h4>
                              <img src="/assets/image/arrow-right.svg" style={{width:'30px'}} alt="" />
                           </div>
                        </Link>
                     </div>
                  </div>
               </div>                
               {/*Old data*/}
            </div>
     	</>
		);
}