import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { Header } from './Header.js';
import { Footer } from './Footer.js';
import { Home } from './Home.js';
import { Data } from './Data.js';
import { Upcoming } from './Upcoming.js';
import { Winning } from './Winning.js';
import { Commission } from './Commision.js';
import { Inplay } from './Inplay.js';
import { Ledger } from './Ledger.js';
import { Password } from './Password.js';
import { Rules } from './Rules.js';
import { NoPage } from './NoPage.js';
export function Myroute(){
	return(
		<>
			<Header />
	          <Routes>
	            {/*<Route path="/" element={<Login />} />*/}
	            <Route path="/" element={<Home />} />
	            <Route path="Upcoming" element={<Upcoming />} />
	            <Route path="Data" element={<Data />} />
	            <Route path="Winning" element={<Winning />} />
	            <Route path="Commission" element={<Commission />} />
	            <Route path="Inplay" element={<Inplay />} />
	            <Route path="Ledger" element={<Ledger />} />
	            <Route path="Password" element={<Password />} />
	            <Route path="Rules" element={<Rules />} />
	            <Route path="/NoPage" element={<NoPage />} />
	            <Route path="*" element={<NoPage />} />
	          </Routes>
	        <Footer />
        </>
		);
}