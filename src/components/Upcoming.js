export function Upcoming(){
	return(
		<>
		{/*body*/}
         <div className="content py-5 mt-2 mb-5">
            <div className="container px-sm-2">
               {/*cricket and virtual*/}
               <div className="row">

                  <div className="col-md-6 px-1">

                     <div className="m-card mb-2">
                        
                        <div className="card-left">
                           <div className="d-flex justify-content-between align-items-center">
                              <div className="upcoming">
                              Upcoming
                           </div>
                           <div className="match_time">
                              Oct 29, 12:40 PM
                           </div>
                           </div>
                           <div className="m_ttl">
                             India v England
                            </div>
                        </div>

                        <div className="card-right-img">
                           <img src="/assets/image/wc.png" alt=""/>
                        </div>
                     </div> 
                  </div>

                  <div className="col-md-6 px-1">

                      <div className="m-card mb-2">
                        
                        <div className="card-left">
                           <div className="d-flex justify-content-between align-items-center">
                              <div className="upcoming">
                              Upcoming
                           </div>
                           <div className="match_time">
                              Oct 29, 12:40 PM
                           </div>
                           </div>
                           <div className="m_ttl">
                             Adelaide Strikers WBBL v Brisbane Heat WBBL
                            </div>
                        </div>

                        <div className="card-right-img">
                           <img src="/assets/image/virtual.png" alt="" />
                        </div>
                     </div> 
                  </div> 

                  <div className="col-md-6 px-1">

                     <div className="m-card mb-2">
                        
                        <div className="card-left">
                           <div className="d-flex justify-content-between align-items-center">
                              <div className="upcoming">
                              Upcoming
                           </div>
                           <div className="match_time">
                              Oct 29, 12:40 PM
                           </div>
                           </div>
                           <div className="m_ttl">
                             India v England
                            </div>
                        </div>

                        <div className="card-right-img">
                           <img src="/assets/image/wc.png" alt="" />
                        </div>
                     </div> 
                  </div>

                  <div className="col-md-6 px-1">

                      <div className="m-card mb-2">
                        
                        <div className="card-left">
                           <div className="d-flex justify-content-between align-items-center">
                              <div className="upcoming">
                              Upcoming
                           </div>
                           <div className="match_time">
                              Oct 29, 12:40 PM
                           </div>
                           </div>
                           <div className="m_ttl">
                             Adelaide Strikers WBBL v Brisbane Heat WBBL
                            </div>
                        </div>

                        <div className="card-right-img">
                           <img src="/assets/image/virtual.png" alt="" />
                        </div>
                     </div> 
                  </div>
               </div>
               {/*cricket and virtual*/}
            </div>
         </div>
       {/*body*/}
        </>
		);
}