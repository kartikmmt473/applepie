import { Link } from 'react-router-dom';
export function Home(){
	return(
		<>

		 {/*body*/}
         <div className="content py-5 mt-2 mb-5">
            <div className="container px-sm-2">
                cricket and virtual
               <div className="row">

                  <Link to="Inplay" className="col-md-6 px-1">

                     <div className="m-card mb-2">
                        
                        <div className="card-left">
                           <div className="d-flex justify-content-between align-items-center">
                              <div className="live">
                              Live
                           </div>
                           <div className="match_time">
                              Oct 29, 12:40 PM
                           </div>
                           </div>
                           <div className="m_ttl">
                             India v England
                            </div>
                        </div>

                        <div className="card-right-img">
                           <img src="/assets/image/wc.png" alt=""/>
                        </div>
                     </div> 
                  </Link>

                  <Link to="Inplay" className="col-md-6 px-1">

                      <div className="m-card mb-2">
                        
                        <div className="card-left">
                           <div className="d-flex justify-content-between align-items-center">
                              <div className="live">
                              Live
                           </div>
                           <div className="match_time">
                              Oct 29, 12:40 PM
                           </div>
                           </div>
                           <div className="m_ttl">
                             Adelaide Strikers WBBL v Brisbane Heat WBBL
                            </div>
                        </div>

                        <div className="card-right-img">
                           <img src="/assets/image/virtual.png" alt=""/>
                        </div>
                     </div> 
                  </Link> 

                  <Link to="Inplay" className="col-md-6 px-1">

                     <div className="m-card mb-2">
                        
                        <div className="card-left">
                           <div className="d-flex justify-content-between align-items-center">
                              <div className="live">
                              Live
                           </div>
                           <div className="match_time">
                              Oct 29, 12:40 PM
                           </div>
                           </div>
                           <div className="m_ttl">
                             India v England
                            </div>
                        </div>

                        <div className="card-right-img">
                           <img src="/assets/image/wc.png" alt=""/>
                        </div>
                     </div> 
                  </Link>

                  <Link to="Inplay" className="col-md-6 px-1">

                      <div className="m-card mb-2">
                        
                        <div className="card-left">
                           <div className="d-flex justify-content-between align-items-center">
                              <div className="live">
                              Live
                           </div>
                           <div className="match_time">
                              Oct 29, 12:40 PM
                           </div>
                           </div>
                           <div className="m_ttl">
                             Adelaide Strikers WBBL v Brisbane Heat WBBL
                            </div>
                        </div>

                        <div className="card-right-img">
                           <img src="/assets/image/virtual.png" alt=""/>
                        </div>
                     </div> 
                  </Link>
               </div>

                {/*cricket and virtual*/}

                {/*casino game*/}
               <div className="row no-gutters">
                  <div className="col-4 col-md-3 p-1">
                     <a href="/">
                        <img src="/assets/image/4444100001.jpg" className="img-fluid" alt="" />
                     </a>
                     </div>
                      <div className="col-4 col-md-3 p-1">

                     <a href="/">
                        <img src="/assets/image/4444100002.jpg" className="img-fluid" alt="" />
                     </a>
                     </div>
                      <div className="col-4 col-md-3 p-1">
                     <a href="/">
                        <img src="/assets/image/4444100003.jpg" className="img-fluid" alt="" />
                     </a>
                     </div>
                      <div className="col-4 col-md-3 p-1">
                     <a href="/">
                        <img src="/assets/image/4444100004.jpg" className="img-fluid" alt="" />
                     </a>
                     </div>
                      <div className="col-4 col-md-3 p-1">
                     <a href="/">
                        <img src="/assets/image/4444100005.jpg" className="img-fluid" alt="" />
                     </a>
                     </div>
                      <div className="col-4 col-md-3 p-1">
                     <a href="/">
                        <img src="/assets/image/4444100006.jpg" className="img-fluid" alt="" />
                     </a>
                     </div>
                      <div className="col-4 col-md-3 p-1">
                     <a href="/">
                        <img src="/assets/image/4444100007.jpg" className="img-fluid" alt="" />
                     </a>
                     </div>
                      <div className="col-4 col-md-3 p-1">
                     <a href="/">
                        <img src="/assets/image/4444100008.jpg" className="img-fluid" alt="" />
                     </a>
                  </div>
               </div>
                {/*casino game*/}
            </div>
         </div>
        {/*body*/}
         </>
		);
}