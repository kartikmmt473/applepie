import '../style.css';
export function Login(){
	return(
		<>
		{/******************** header ********************/}
      <section className="first_section">
         <header className="top-header">
            <nav className="navbar navbar-default navbar-expand-lg navbar-dark p-0 fixed-top">
               <a className="navbar-brand ml-3" href="/">
               </a>
               <button className="navbar-toggler text-dark mr-5" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span className="navbar-toggler-icon"></span>
               </button>
               <div className="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul className="navbar-nav text-uppercase p-1">
                     <li className="nav-item">
                        <h4 className="nav-link text-white ml-4" href="/">BETHUB</h4>
                     </li>
                     <li className="nav-item8 mr-2">
                        <button type="button" className="btn btn-primary rounded-pill mt-2 login ">LOGIN</button>
                     </li>
                     <li className="nav-item7">
                        <button type="button" className="btn btn-secondary rounded-pill mt-2">REGISTER</button>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
      </section>
      <section className="third_section p-3 mt-5">
         <div className="card-container card11 p-3">
            <div className="card card12">
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">World Cup 2023</div>
                  <div className="col-6 col-md-4">Tomorrow </div>
               </div>
               <div className="container">
                  <div className="row CardRow">
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                     <div className="w-100"></div>
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                  </div>
               </div>
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">Start At <span>2:00 PM</span></div>
                  <div className="col-6 col-md-4">
                     <div className="embed-responsive embed-responsive-16by9">
                        <iframe id="frame1" className="embed-responsive-item" src="https://youtu.be/F80kqi5z2SQ?si=wXS4dKll2W0eFtJm" allowfullscreen></iframe>
                     </div>
                  </div>
               </div>
            </div>
            <div className="card card12">
           
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">World Cup 2023</div>
                  <div className="col-6 col-md-4">Tomorrow</div>
               </div>
               <div className="container">
                  <div className="row CardRow">
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                   
                     <div className="w-100"></div>
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                  </div>
               </div>
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">Start At <span>2:00 PM</span></div>
                  <div className="col-6 col-md-4">
                     <div className="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" title="frame2" src="https://youtu.be/F80kqi5z2SQ?si=wXS4dKll2W0eFtJm" allowfullscreen></iframe>
                     </div>
                  </div>
               </div>
            </div>
            <div className="card card12">
             
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">World Cup 2023</div>
                  <div className="col-6 col-md-4">Tomorrow</div>
               </div>
               <div className="container">
                  <div className="row CardRow">
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
              
                     <div className="w-100"></div>
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                  </div>
               </div>
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">Start At <span>2:00 PM</span></div>
                  <div className="col-6 col-md-4">
                     <div className="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://youtu.be/F80kqi5z2SQ?si=wXS4dKll2W0eFtJm" allowfullscreen></iframe>
                     </div>
                  </div>
               </div>
            </div>
            <div className="card card12">
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">World Cup 2023</div>
                  <div className="col-6 col-md-4">Tomorrow</div>
               </div>
               <div className="container">
                  <div className="row CardRow">
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
        
                     <div className="w-100"></div>
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                  </div>
               </div>
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">Start At <span>2:00 PM</span></div>
                  <div className="col-6 col-md-4">
                     <div className="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://youtu.be/F80kqi5z2SQ?si=wXS4dKll2W0eFtJm" allowfullscreen></iframe>
                     </div>
                  </div>
               </div>
            </div>
             <div className="card card12">
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">World Cup 2023</div>
                  <div className="col-6 col-md-4">Tomorrow</div>
               </div>
               <div className="container">
                  <div className="row CardRow">
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
        
                     <div className="w-100"></div>
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                  </div>
               </div>
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">Start At <span>2:00 PM</span></div>
                  <div className="col-6 col-md-4">
                     <div className="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://youtu.be/F80kqi5z2SQ?si=wXS4dKll2W0eFtJm" allowfullscreen></iframe>
                     </div>
                  </div>
               </div>
            </div>
             <div className="card card12">
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">World Cup 2023</div>
                  <div className="col-6 col-md-4">Tomorrow</div>
               </div>
               <div className="container">
                  <div className="row CardRow">
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
        
                     <div className="w-100"></div>
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                  </div>
               </div>
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">Start At <span>2:00 PM</span></div>
                  <div className="col-6 col-md-4">
                     <div className="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://youtu.be/F80kqi5z2SQ?si=wXS4dKll2W0eFtJm" allowfullscreen></iframe>
                     </div>
                  </div>
               </div>
            </div>
            <div className="card card12">
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">World Cup 2023</div>
                  <div className="col-6 col-md-4">Tomorrow</div>
               </div>
               <div className="container">
                  <div className="row CardRow">
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                     <div className="w-100"></div>
                     <div className="col-6 col-sm-3">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col-6 col-sm-3 country">INDIA</div>
                  </div>
               </div>
               <div className="row no-gutters p-2">
                  <div className="col-sm-6 col-md-8">Start At <span>2:00 PM</span></div>
                  <div className="col-6 col-md-4">
                     <div className="embed-responsive embed-responsive-16by9">
                        <iframe className="embed-responsive-item" src="https://youtu.be/F80kqi5z2SQ?si=wXS4dKll2W0eFtJm" allowfullscreen></iframe>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </section>
      <section className="six_section mt-5">
      </section>
      <section className="four_section ">
         <div className="container mt-2">
            <div className="">
               <div className="row">
                  <div className="col-md-2 class2">
                     <img src="/assets/image/2.png" className="card-img-top" alt="..." />
                     <div className="card-body">
                        <h5 className="card-title text-white">ICE HOKEY</h5>
                     </div>
                  </div>
                  <div className="col-md-2">
                     <img src="/assets/image/2.png" className="card-img-top" alt="..." />
                     <div className="card-body">
                        <h5 className="card-title text-white">VOLLEYBALL</h5>
                     </div>
                  </div>
                  <div className="col-md-2">
                     <img src="/assets/image/2.png" className="card-img-top" alt="..." />
                     <div className="card-body">
                        <h5 className="card-title text-white">BASKETBALL</h5>
                     </div>
                  </div>
                  <div className="col-md-2">
                     <img src="/assets/image/2.png" className="card-img-top" alt="..." />
                     <div className="card-body">
                        <h5 className="card-title text-white">TENNIS</h5>
                     </div>
                  </div>
                  <div className="col-md-2">
                     <img src="/assets/image/2.png" className="card-img-top" alt="..." />
                     <div className="card-body">
                        <h5 className="card-title text-white">CRICKET</h5>
                     </div>
                  </div>
                  <div className="col-md-2">
                     <img src="/assets/image/2.png" className="card-img-top" alt="..." />
                     <div className="card-body">
                        <h5 className="card-title text-white">FOOTBALL</h5>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section className="Five_section mt-5">
         <div className="buttom mt-5 ml-5 p-3 ">
            <img className="img1 ml-4" src="/assets/image/cup1.png" alt="..."/>
            <div className="col-md-4 btn1 btn btn-secondary text-info">Cricket (15)
               <i className="fa fa-long-arrow-right ml-4"></i>
            </div>
            <div className="col-md-4 btn1 offset-md-4 btn btn-primary text-info">See All
               <i className="fa fa-long-arrow-right ml-4"></i>
            </div>
         </div>
      </section>
      <section className="six_section">
         <div className="row class1 grid main">
            <div className="col-sm-6 ">
               <div className="card card13">
                  <div className="card-body text-center">
                     <div className="container 2">
                        <div className="dropdown">
                           <button className="btn btn-secondary dropdown-toggle mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Dropdown button
                           </button>
                           <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a className="dropdown-item" href="/">Action</a>
                              <a className="dropdown-item" href="/">Another action</a>
                              <a className="dropdown-item" href="/">Something else here</a>
                           </div>
                        </div>
                     <input className="btn btn-secondary  p-2" type="date" id="date-buttom" ></input>
                        <div id="carouselExampleIndicators" className="carousel slide mt-2" data-ride="carousel">
   <ol className="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      {/*Add more indicators if needed*/}
   </ol>
   <div className="carousel-inner">
      <div className="carousel-item active">
                                 <button>1th<br />march</button>
                                 <button>2th<br />march</button>
                                 <button>3th<br />march</button>
                                 <button>4th<br />march</button>
                                 <button>5th<br />march</button>
                                 <button>6th<br />march</button>
                                 <button>7th<br />march</button>
      </div>
      <div className="carousel-item">
                                  <button>8th<br />march</button>
                                 <button>9th<br />march</button>
                                 <button>10th<br />march</button>
                                 <button>11th<br />march</button>
                                 <button>12th<br />march</button>
                                 <button>13th<br />march</button>
                                 <button>14th<br />march</button>
      </div>
      {/*Add more carousel items as needed*/}
   </div>
   <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span className="carousel-control-prev-icon" aria-hidden="true"></span>
      <span className="sr-only">Previous</span>
   </a>
   <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span className="carousel-control-next-icon" aria-hidden="true"></span>
      <span className="sr-only">Next</span>
   </a>
</div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div className="row class1 grid mt-2">
            <div className="col-sm-6">
               <div className="card card2">
                  <div className="card-body">
                     <h5 className="card-title text-center">Friday 20th October</h5>
                  </div>
                  <div className="row">
                     <div className="col">
                        <h6>India</h6>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <button>2:00</button>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <h6>India</h6>
                     </div>
                  </div>
                  <div className="row justify-content-between">
                     <div type="button"  className="col-4">
                        <img className="img2 buttom" src="/assets/image/cup1.png" alt=""/>
                        Watch Center
                     </div>
                     <div type="button"  className="col-4"><i className="fa fa-play-circle-o mr-1" style={{fontSize:'22px'}}></i>
                        Live Steam
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div className="row class1 grid mt-2">
            <div className="col-sm-6">
               <div className="card card2">
                  <div className="card-body">
                     <h5 className="card-title text-center">Friday 20th October</h5>
                  </div>
                  <div className="row">
                     <div className="col">
                        <h6>India</h6>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <button>2:00</button>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <h6>India</h6>
                     </div>
                  </div>
                  <div className="row justify-content-between">
                     <div type="button" className="col-4">
                        <img className="img2" src="/assets/image/cup1.png" alt=""/>
                        Watch Center
                     </div>
                     <div type="button"  className="col-4"><i className="fa fa-play-circle-o mr-1" style={{fontSize:'22px'}}></i>
                        Live Steam
                     </div>
                  </div>
               </div>
            </div>
            <div className="col-sm-6 mt-2">
               <div className="card card2">
                  <div className="card-body">
                     <h5 className="card-title text-center">Friday 20th October</h5>
                  </div>
                  <div className="row">
                     <div className="col">
                        <h6>India</h6>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <button>2:00</button>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <h6>India</h6>
                     </div>
                  </div>
                  <div className="row justify-content-between">
                     <div type="button"  className="col-4">
                        <img className="img2" src="/assets/image/cup1.png" alt="" />
                        Watch Center1
                     </div>
                     <div type="button" className="col-4"><i className="fa fa-play-circle-o mr-1" style={{fontSize:'22px'}}></i>
                        Live Steam
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div className="row class1 grid mt-2">
            <div className="col-sm-6">
               <div className="card card2">
                  <div className="card-body">
                     <h5 className="card-title text-center">Friday 20th October</h5>
                  </div>
                  <div className="row">
                     <div className="col">
                        <h6>India</h6>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <button>2:00</button>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <h6>India</h6>
                     </div>
                  </div>
                  <div className="row justify-content-between">
                     <div type="button" className="col-4">
                        <img className="img2" src="/assets/image/cup1.png" alt="" />
                        Watch Center
                     </div>
                     <div type="button"  className="col-4"><i className="fa fa-play-circle-o mr-1" style={{fontSize:'22px'}}></i>
                        Live Steam
                     </div>
                  </div>
               </div>
            </div>
            <div className="col-sm-6 mt-2">
               <div className="card card2">
                  <div className="card-body">
                     <h5 className="card-title text-center">Friday 20th October</h5>
                  </div>
                  <div className="row">
                     <div className="col">
                        <h6>India</h6>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <button>2:00</button>
                     </div>
                     <div className="col">
                        <img src="/assets/image/india.jpeg" className="rounded mx-auto d-block" alt="..." />
                     </div>
                     <div className="col">
                        <h6>India</h6>
                     </div>
                  </div>
                  <div className="row justify-content-between">
                     <div type="button"  className="col-4">
                        <img className="img2" src="/assets/image/cup1.png" alt="" />
                        Watch Center1
                     </div>
                     <div type="button" className="col-4"><i className="fa fa-play-circle-o mr-1" style={{fontSize:'22px'}}></i>
                        Live Steam
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section className="eight_section mt-2">
         <div className="buttom mt-5 ml-5 ">
            <img className="img1 ml-3" src="/assets/image/cup1.png" alt="" />
            <div className="col-md-4 btn1 btn btn-secondary text-info p-1">See All Sports
               <i className="fa fa-long-arrow-right ml-4"></i>
            </div>
         </div>
      </section>
      <section className="eight_section mt-5   ">
         <div className="container">
            <div className="row">
               <div className="col-lg-6 col-md-12">
                  <img src="/assets/image/5.png" className="card-img-top_one img-fluid" alt="..." />
               </div>
               <div className="col-lg-6 col-md-12">
                  <div className="container">
                     <div className="row">
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/4.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/5.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/4.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/5.png" className="card-img-top img-fluid" alt="..." /></div>
                     </div>
                     <div className="row">
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/4.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/5.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/4.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/5.png" className="card-img-top img-fluid" alt="..." /></div>
                     </div>
                     <div className="row">
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/4.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/5.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/4.png" className="card-img-top img-fluid" alt="..." /></div>
                        <div className="col-6 col-md-3 p-1"><img src="/assets/image/5.png" className="card-img-top img-fluid" alt="..." /></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      {/*</section>*/}
      <section className="seven_section mt-5">
         <div className="container">
            <div className="row">
               <div className="col mr-3">
                  <img src="/assets/image/3.png" className="card-img-top_o m-2" alt="" />
               </div>
               <div className="col-md-4">
                  <div className="container">
                     <div className="row">
                        <div className="col-md-3 p-2">
                           <img src="/assets/image/1.png" className="card-img-top_o" alt="..." />
                        </div>
                        <div className="col p-2">
                           England vs Sri Lanka Live Score, World Cup 2023: England in a spot of bother against Sri Lanka
                        </div>
                     </div>
                     <div className="row">
                        <div className="col-md-3 p-2">
                           <img src="/assets/image/1.png" className="card-img-top_o" alt="..." />
                        </div>
                        <div className="col-md-9 p-2">
                           England vs Sri Lanka Live Score, World Cup 2023: England in a spot of bother against Sri Lanka
                        </div>
                     </div>
                     <div className="row">
                        <div className="col-md-3 p-2">
                           <img src="/assets/image/1.png" className="card-img-top_o" alt="..." />
                        </div>
                        <div className="col p-2">
                           England vs Sri Lanka Live Score, World Cup 2023: England in a spot of bother against Sri Lanka
                        </div>
                     </div>
                     <div className="row">
                        <div className="col-md-3 p-2">
                           <img src="/assets/image/1.png" className="card-img-top_o" alt="..." />
                        </div>
                        <div className="col-md-9 p-2">
                           England vs Sri Lanka Live Score, World Cup 2023: England in a spot of bother against Sri Lanka
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section className="eight_section">
         <div className="buttom mt-5 ml-5 ">
            <img className="img1 ml-3" src="/assets/image/cup1.png" alt="" />
            <div className="col-md-4 btn1 btn btn-secondary p-1 text-info">Upcoming Match
               <i className="fa fa-long-arrow-right"></i>
            </div>
         </div>
      </section>
      {/*<section className="12_section">
         <h1>World Cup Schedule</h1>
         <div className="schedule">
         <p className="match-time">16:00</p>
         <p className="match-date">Wed 18 October (Local)</p>
         <p className="match-info">New Zealand VS Afghanistan</p>
         <p className="match-info">Ov : 50/50 Group Stage</p>
         <p className="match-venue">MA Chindambaram Stadium, Checnnai</p>
         </div>
         </section> */}
      <section className="11_section mt-5">
         <div className="container">
            <div className="row">
               <div className="col-sm Learn text-white">
                  <button type="button" className="btn btn-secondary text-left">Learn More</button>
               </div>
               <div className="col-sm stoke text-white">
                  Stoke | Bet
               </div>
               <div className="col-sm gethelp text-white">
                  <button type="button" className="btn btn-primary .get">GET HELP</button>
               </div>
            </div>
         </div>
      </section>
 	  	<section className="seven_section">
         <div className="container">
	         <div className="row justify-content-md-center">
	            <div className="col col-lg-2">
	               <i className="bi bi-arrow-90deg-right fa-9x"></i>
	            </div>
	            <div className="col-md-auto">
	               <img src="/assets/image/4.png" className="rounded mx-auto d-block side-image" alt="..." />
	               <h5 align="center">Oppen An Account</h5>
	               <p align="Center">Create an account. Enter your<br/>personal details into the sign-up form<br/>& click the 'Register' Buttom.</p>
	            </div>
	         </div>
         </div>
         <div className="container">
	         <div className="row justify-content-md-center">
	            <div className="col-md-auto">
	               <img src="/assets/image/4.png" className="rounded mx-auto d-block side-image" alt="..." />
	               <h5 align="center">Oppen An Account</h5>
	               <p align="Center">Create an account. Enter your<br/>personal details into the sign-up form<br/>& click the 'Register' Buttom.</p>
	            </div>
	            <div className="col-md-auto mid-image">
	               <img src="/assets/image/last.png" className="rounded mx-auto d-block mid-image" alt="..." />
	            </div>
	            <div className="col-md-auto">
	               <img src="/assets/image/4.png" className="rounded mx-auto d-block side-image" alt="..." />
	               <h5 align="center">Oppen An Account</h5>
	               <p align="Center">Create an account. Enter your<br/>personal details into the sign-up form<br/>& click the 'Register' Buttom.</p>
	            </div>
	         </div>
         </div>
         <div className="container">
	         <div className="row justify-content-md-center">
	            <div className="col col-lg-2 ">
	          
	               <i className="bi bi-arrow-90deg-up fa-9x mb-5"></i>
	            </div>
	            <div className="col-md-auto">
	               <img src="/assets/image/4.png" className="rounded mx-auto d-block side-image" alt="..." />
	               <h5 align="center">Oppen An Account</h5>
	               <p align="Center">Create an account. Enter your<br/>personal details into the sign-up form<br/>& click the 'Register' Buttom.</p>
	            </div>
	            <div className="col col-lg-2 mb-5">
	               <i className="bi bi-arrow-return-left fa-9x"></i>
	               {/*<i className="bi bi-arrow-return-right"></i>*/}
	            </div>
	         </div>
         </div>
      	</section>
       {/************************ footer start ************************** */}
      <section className="nine_section mt-5">
         <footer>
            <div className="card mt-5">
               <div className="row mb-4 ">
                  <div className="col">
                     <div className="footer-text pull-left">
                        <p className="card-text text-white">Help Center</p>
                        <div className="dfd mt-2 text-white">
                           <label>if yout have any question?  </label>
                           <div className="nav-item8">
                              <button type="button" className="btn btn-primary .get">GET ANSWERS</button>
                           </div>
                        </div>
                        <div className="social mt-4 mb-3 fa-lg rounded-circle">
                           <i className="fa fa-facebook-official"></i>
                           <i className="fa fa-instagram"></i>
                           <i className="fa fa-twitter"></i>
                           <i className="fa fa-linkedin-square"></i>
                        </div>
                     </div>
                  </div>
                  <div className="col-md-2 col-sm-4 col-xs-4">
                     <h5 className="heading text-white">Games</h5>
                     <ul>
                        <li>
                           <label>Slots</label>
                        </li>
                        <li>
                           <label>Live Casino</label>
                        </li>
                        <li>
                           <label>Instant Games</label>
                        </li>
                        <li>
                           <label>Jackpot Games</label>
                        </li>
                     </ul>
                  </div>
                  <div className="col-md-2 col-sm-4 col-xs-4">
                     <h5 className="heading text-white">About</h5>
                     <ul className="card-text">
                        <li>
                           <label>Abohut Us</label>
                        </li>
                        <li>
                           <label>Promotion</label>
                        </li>
                        <li>
                           <label>Vip</label>
                        </li>
                        <li>
                           <label>Help Center</label>
                        </li>
                        <li>
                           <label>Afffiliate Progaram</label>
                        </li>
                     </ul>
                  </div>
                  <div className="col-md-2 col-sm-4 col-xs-4">
                     <h5 className="heading text-white">Legal Information</h5>
                     <ul className="card-text">
                        <li>
                           <label>General Terms & Conditions</label>
                        </li>
                        <li>
                           <label>Responsible Gaming Policy</label>
                        </li>
                        <li>
                           <label>Privacy and Cookies Policy</label>
                        </li>
                        <li>
                           <label>Payment Method</label>
                        </li>
                     </ul>
                  </div>
                  <div className="col-md-2 col-sm-4 col-xs-4">
                     {/*<h5 className="heading text-white">Legal Information</h5>*/}
                     <ul className="card-text">
                        <img src="/assets/01.png" className="card-img-top12" alt="..." />
                        <li>
                           <label>Responsible Gaming Policy</label>
                        </li>
                        <li>
                           <label>Privacy and Cookies Policy</label>
                        </li>
                        <li>
                           <label>Payment Method</label>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </footer>
      </section>
      <section className="ten_section">
         <div className="divider mb-4">
            <div className="foter1 text-center text-white">
               <div className="copyring">copyright 2023. All Right Reserved
               {/*<h>copyright 2023. All Right Reserved</h>*/}
               </div>
            </div>
         </div>
      </section>
      {/************************ footer end ***************************/}
      </>
		);
}