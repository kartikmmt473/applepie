import { Link } from 'react-router-dom';
export function NoPage(){
	return(
		<>
		   <div className="container-fluid bg-dark text-white">
		      <div className="row d-flex justify-content-center align-items-center height-vh">
		         <div className="col-lg-6 col-12">
		            <div className="col-md-12">
		               <img src="/assets/image/image.png" width="100%" alt="" />
		            </div>
		         </div>
		         <div className="col-lg-6 col-12">
		            <div className="col-12 d-flex flex-column justify-content-center align-items-center">
		               <h1 className="main-heading">404</h1>
		               <h2>we couldn't find this page.</h2>
		               <div className="text-center mt-4 mb-5">
		                  <button className="btn btn-success px-3 mr-2">
		                  	<Link to="/" className="text-decoration-none text-white no-page-home">
		                  		<i className="fa fa-home"></i> Home
		                  	</Link>
		                  </button>
		                  <button className="btn btn-success px-3 mr-2"><i className="fa fa-phone"></i> Contact</button>
		                  <button className="btn btn-success px-3 mr-2"><i className="fa fa-info"></i> Report</button>
		               </div>
		            </div>
		         </div>
		      </div>
		   </div>
		</>
		);
}