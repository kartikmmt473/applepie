export function Ledger(){
	return(
		<>
			<div class="content py-5 mt-2 mb-5">
            <div class="container px-sm-2">
               {/*Rules*/}
               <div class="row">
                  <div class="col-md-6 m-auto">
                  	<div class="card p-2 shadow border-0 w-100 mb-3">
                  		<img src="/assets/image/ledger.png" class="img-forget" alt="" />
                  		<div class="forget_password">
                              <h3 class="my-2 text-center">My Ledger</h3>
                             </div>

                           <div class="client_ledger">
                              <table class="table mb-4 ">
                                 <thead>
                                    <tr>
                                       <th>MATCH NAME</th>
                                       <th class="text-center">WON BY</th>
                                       <th class="text-center">WON</th>
                                       <th class="text-center">LOST</th>
                                       <th class="text-right">BALANCE</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>Lucky 7 <span class="d-block  color-2 small">(Oct 3, 12:00 AM)</span></td>
                                       <td class="text-center"><span><img src="/assets/image/trophy.png" alt="" /></span></td>
                                       <td class="text-center">0</td>
                                       <td class="text-center"><span class="text-danger">-100</span></td>
                                       <td class="text-right"><span class="">-100</span></td>
                                    </tr>
                                    <tr>
                                       <td>Dragon Tiger <span class="d-block  color-2 small">(Oct 4, 2:00 AM)</span></td>
                                       <td class="text-center"><span><img src="/assets/image/trophy.png" alt="" /></span></td>
                                       <td class="text-center"><span class="text-success">1000</span></td>
                                       <td class="text-center"><span class="text-danger">0</span></td>
                                       <td class="text-right"><span class="">900</span></td>
                                    </tr>
                                    <tr>
                                       <td>India v Australia <span class="d-block  color-2 small">(Oct 8, 2:00 PM)</span></td>
                                       <td class="text-center"><img src="/assets/image/trophy.png" alt="" /><span> IND </span></td>
                                       <td class="text-center"><span class="text-success">1500</span></td>
                                       <td class="text-center"><span class="text-danger">0</span></td>
                                       <td class="text-right"><span class="">2400</span></td>
                                    </tr>
                                    <tr>
                                       <td class="" colspan="4"><h5 class="mb-0">Total</h5></td>
                                       <td class="text-right"><span class="text-success h5 mb-0">2400</span></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>

                  	</div>
                  </div>
               </div> 
                  
               {/*Rules*/}

               
            </div>
         </div>
		</>

		);
}