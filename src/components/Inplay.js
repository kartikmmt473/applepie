export function Inplay(){
	return(
		<>
			<div className="content py-5 mt-2 mb-5">
            <div className="container px-sm-2">
               {/*cricket and virtual*/}
               <div className="row">
                  <div className="col-md-12 px-1">
                     {/*live tv*/}
                     <div className="live_tv d-flex justify-content-between">
                        <div className="score_list position-relative"><img src="/assets/image/score.svg" alt="" /> <span>SCORE</span></div>
                        <div className="l_tv">
                           <img src="/assets/image/tv.svg" className="img-fluid" alt="" />
                           <input type="checkbox" checked className="style3" id="chktv" />
                        </div>
                        
                     </div>
                     <div className="show_tv_panel" id="dvtv" >
                        <img src="/assets/image/live-tv.png" className="img-fluid" alt="" />
                     </div>
                     {/*live tv*/}

                     {/*inplay Lagai khai*/}
                        <div className="match_odds">
                           <table className="table shadow ">
                              <thead>
                                 <tr>
                                    <th >
                                       <div className="d-flex justify-content-between color-3" ><span>MIN : 500</span><span>MAX : 2,00,000</span></div>
                                    </th>
                                    <th className="text-center ">LAGAI</th>
                                    <th className="text-center">KHAI</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Perth Scorchers WBBL</td>
                                    <td className="text-center position-relative lagai">0.50 <span className="suspen">SUSPENDED</span></td>
                                    <td className="text-center position-relative khai">0.52 <span className="suspen">SUSPENDED</span></td>
                                 </tr>
                                 <tr>
                                    <td>Melbourne Renegades WBBL</td>
                                    <td className="text-center position-relative lagai">0.97</td>
                                    <td className="text-center position-relative khai">0.98 </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     {/*inplay Lagai khai*/}
                     {/*inplay session not yes*/}
                        <div className="match_odds match_session">
                           <table className="table shadow mb-0">
                              <thead>
                                 <tr>
                                    <th >
                                       <div className="d-flex justify-content-between color-3" >SESSION</div>
                                    </th>
                                    <th className="text-center ">NOT</th>
                                    <th className="text-center">YES</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>6 over runs SS W (AS W vs    SS W) adv <span className="bet_limit"> MIN:200 - MAX:50,000</span></td>
                                    <td className="text-center position-relative khai"><span className="d-block">40</span>  <span className="return_rate">100</span></td>
                                    <td className="text-center position-relative  lagai"><span className="d-block">42</span> <span className="return_rate">100</span></td>
                                 </tr>
                                 <tr>
                                    <td>Match 1st over run(AS W vs SSW)adv <span className="bet_limit">MIN:200 - MAX:50,000</span></td>
                                    <td className="text-center position-relative  khai"><span className="d-block">5</span> <span className="return_rate">100</span></td>
                                    <td className="text-center position-relative lagai"><span className="d-block">6</span> <span className="return_rate">100</span></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     {/*inplay session not yes*/}
                  </div>
               </div>

            </div>
         </div>
		</>
		);
}