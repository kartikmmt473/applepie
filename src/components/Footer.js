import { NavLink, Link } from "react-router-dom";
import { showDiv, closeDiv } from "./Functions.js";
export function Footer() {
	return(
		<>
		{/*user data*/}
         <div className="userdata" id="userdata" style={{display:'none'}}>
            <div className="userdata_list">
               <div onClick={closeDiv} className="closes"><img src="/assets/image/close.svg" alt="" /></div>
               <ul>
                  <li><Link to="/Ledger"><img src="/assets/image/ledger.svg" alt="" />My Ledger</Link></li>
                  <li><Link to="/Commission"><img src="/assets/image/commission.svg" alt="" />My Commission</Link></li>
                  <li><Link to="/rules"><img src="/assets/image/rules.svg" alt="" />Rules</Link></li>
                  <li><Link to="/data"><img src="/assets/image/data.svg" alt="" />OLD DATA</Link></li>
                  <li><Link to="/password"><img src="/assets/image/password.svg" alt="" />Change Password</Link></li>
                  <li><Link to="/"><img src="/assets/image/logout.svg" alt="" />Logout</Link></li>
               </ul>
            </div>
         </div>
       {/*user data*/}
   		<div className="footer">
           <ul>
              <li><NavLink exact activeClassName="active" to="/"><img src="assets/image/in-play.svg" alt=""/><span>In Play</span></NavLink></li>
              <li><NavLink activeClassName="active" to="/upcoming"><img src="assets/image/all-sports.svg" alt=""/><span>Upcoming</span></NavLink></li>
              <li><NavLink activeClassName="active" to="/winning"><img src="assets/image/winning.svg" alt=""/><span>Profit & Loss</span></NavLink></li>
              <li><Link to="/" onClick={showDiv} ><img src="assets/image/user.svg" alt=""/><span>CL36924</span></Link></li>
           </ul>
        </div>
        </>
		);
}