export function Winning() {
	return(
		<>
			{/*body*/}
	         <div className="content py-5 mt-2 mb-5">
	            <div className="container px-sm-2">
	               {/*Winning and loss history*/}
	               <div className="row" id="self_hide">
	                   <div className="col-md-6 px-1" id="show">
	                     <div className="m-card mb-2" >
	                        <div className="card-left">
	                           <div className="d-flex justify-content-between align-items-center">
	                              <div className="winning_game">
	                               Result
	                           </div>
	                           <div className="match_time">
	                               Oct 29, 12:40 PM
	                           </div>
	                           </div>
	                           <div className="d-flex justify-content-between">
	                              <div className="m_ttl text-dark">
	                                 Adelaide Strikers WBBL v Brisbane Heat WBBL
	                               </div>
	                               <div className="total m-loss">-1000</div>
	                           </div>
	                        </div>
	                        <div className="card-right-img">
	                           <img src="/assets/image/arrow-right.svg" alt="" />
	                        </div>
	                     </div>
	                  </div>
	                  <div className="col-md-6 px-1" >
	                     <div className="m-card mb-2">
	                        
	                        <div className="card-left">
	                           <div className="d-flex justify-content-between align-items-center">
	                              <div className="winning_game">
	                              Result
	                           </div>
	                           <div className="match_time">
	                             Oct 29, 12:40 PM
	                           </div>
	                           </div>
	                           <div className="d-flex justify-content-between">
	                              <div className="m_ttl text-dark">
	                                  Dragon Tiger 
	                               </div>
	                               <div className="total">497</div>
	                           </div>
	                        </div>

	                        <div className="card-right-img">
	                           <img src="/assets/image/arrow-right.svg" alt="" />
	                        </div>
	                     </div> 
	                  </div>
	                 
	               </div>

	               <div className="row" id="target" >
	                  <div className="col-md-6 px-1">
	                     <div className="score_details" id="re_hide">
	                        <div className="match-ttl">
	                           <span>Adelaide Strikers WBBL v Brisbane Heat WBBL</span>
	                           <div className="back">
	                              <img src="/assets/image/arrow-left.svg" alt="" />
	                           </div>
	                        </div>
	                        <div className="session d-flex justify-content-between">
	                           <span>35 over run SL</span>
	                           <span className="text-danger"> -100</span>
	                        </div>
	                        <div className="session d-flex justify-content-between">
	                           <span>34 over run SL</span>
	                           <span className="text-success"> 500</span>
	                        </div>
	                        <div className="all_total d-flex justify-content-between">
	                           <span>Total</span>
	                           <span className="text-success">400</span>
	                        </div>
	                     </div>
	                  </div>
	               </div>

	                  {/*today final profit*/}
	                  <div className="final_profit game-loss">
	                     <span>Total</span>
	                     <span>-503</span>
	                  </div>
	                  {/*today final profit*/}
	               {/*Winning and loss history*/}
	            </div>
	         </div>
	       {/*body*/}
		</>

		);
}