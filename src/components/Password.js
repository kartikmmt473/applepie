export function Password(){
	return(
		<>
			<div class="content py-5 mt-2 mb-5">
            <div class="container px-sm-2">
               {/*change password*/}
               <div class="row">
                  <div class="col-md-6 m-auto text-center">
                      <img src="/assets/image/update_password.png" class="img-forget" alt="" />
                     <div class="card p-2 shadow border-0">
                        <div class="forget_password">
                           <h3 class="mb-3">Change Password</h3>
                          <form>
                              <div class="form-group">
                                 {/*<label for="pass" class="labell">Password</label>*/}
                                 <input id="pass" type="text" class="form-control" name="" placeholder="Enter Password" />
                              </div>
                              <div class="form-group">
                                 {/*<label for="con-pass" class="labell">Confirm Password</label>*/}
                                 <input id="con-pass" type="text" class="form-control" name="" placeholder="Confirm Password" />
                              </div>
                              <div class="d-flex justify-content-between">
                                 <a href="index.html"  class="btn btn-2">Cancel</a>
                                 <button type="submit" class="btn btn-1">Update</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div> 
                  
               {/*change password*/}

               
            </div>
         </div>
		</>
		);
}