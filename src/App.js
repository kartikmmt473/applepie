import './App.css';

// import { Login } from './components/Login.js';
import { BrowserRouter } from "react-router-dom";
import { Myroute } from "./components/Myroute.js";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Myroute></Myroute>
      </BrowserRouter>
    </div>
  );
}

export default App;
